#!/bin/bash

for exam_id in $(cat need_to_add_20230621.csv | grep -v exam_id) ; do
  for dir in $(ls /mnt/bigdata/restricted/bluesky/BBS/mri_library) ; do
    if [ -d /mnt/bigdata/restricted/bluesky/BBS/mri_library/$dir/$exam_id ] ; then
     rsync -av /mnt/bigdata/restricted/bluesky/BBS/mri_library/$dir/$exam_id /mnt/bigdata/scratch/abartnik_dicoms/aging/ --progress
    fi
  done
done

