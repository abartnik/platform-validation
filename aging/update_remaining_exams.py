import xnat
import os
import pandas as pd
import numpy as np
from getpass import getpass


xnat_url = "http://donut.bnac.net:8082"
username = "admin"
passwd = getpass()

val_dir = "/shared/nonrestricted/cbi-development/validation"
ceg_dir = f"{val_dir}/ceg"
aging_dir = f"{val_dir}/aging"

aging_exam_ids = list(pd.read_csv(f"{aging_dir}/exam_ids.csv")['exam_id'])
already_added = []
with xnat.connect(xnat_url, user=username, password=passwd) as session:
    ctevd = session.projects['CTEVD']
    for exam_id, exam_data in ctevd.experiments.items():
        already_added.append(exam_data.label)
    
    aging = session.projects['AGING']
    for exam_id, exam_data in aging.experiments.items():
        already_added.append(exam_data.label)
print(already_added)

need_to_add = []
for exam_id in aging_exam_ids:
    if exam_id not in already_added:
        need_to_add.append(exam_id)

need_to_add_df = pd.DataFrame({'exam_id': need_to_add})
need_to_add_df.to_csv(f"{aging_dir}/need_to_add.csv", index=False)

ctevd_dir = "/shared/nonrestricted/cbi-development/CTEVD/data"
mapped_exam_ids = {}
for subject_id in os.listdir(ctevd_dir):
    subject_path = f"{ctevd_dir}/{subject_id}"

    for exam_id in os.listdir(subject_path):
        if exam_id in aging_exam_ids:
            mapped_exam_ids[subject_id] = []
            if exam_id in need_to_add:
                mapped_exam_ids[subject_id].append(exam_id)

paths_to_add = []
for subject_id, exam_id in mapped_exam_ids.items():

    if len(exam_id) > 0:
        exam_path = f"{ctevd_dir}/{subject_id}/{exam_id[0]}"
        paths_to_add.append(exam_path)

paths_to_add_df = pd.DataFrame({'exam_path': paths_to_add})
paths_to_add_df.to_csv(f"{aging_dir}/paths_to_add.csv", index=False, header=False)

